from flask import Flask
import requests

app_2 = Flask(__name__)

@app_2.route("/")
def home():
    result = "2=>"

    try:
        response = requests.get("http://t21_api_3:5003")
        if not response:
            result += "Error: response is null"
        elif response.status_code != 200:
            result += f"Error: response.status = {response.status_code}"
        else:
            result += response.text
    except Exception as e:
        result += f"Error: {str(e)}"

    return result

if __name__ == "__main__":
    app_2.run()
