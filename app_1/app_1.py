from flask import Flask
import requests

app_1 = Flask(__name__)

@app_1.route("/")
def home():
    result = "1=>"

    try:
        response = requests.get("http://t21_api_2:5002")
        if not response:
            result += "Error: response is null"
        elif response.status_code != 200:
            result += f"Error: response.status = {response.status_code}"
        else:
            result += response.text
    except Exception as e:
        result += f"Error: {str(e)}"

    return result

if __name__ == "__main__":
    app_1.run()
